export const environment = {

    production: true,
    apiUrl: 'http://www.protect.co.za/api',
    configurationsURL:'http:///www.protect.co.za',
    configurationsPort:'80',
    fileUploadspath:'http://www.protect.co.za/InvestorFileUpload/FileUploads',
};
