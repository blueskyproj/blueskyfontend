import { Route } from '@angular/router';
import { FundSummaryComponent } from 'app/modules/admin/fund-summary/fund-summary.component';

export const fundRoutes: Route[] = [
    {
        path     : '',
        component: FundSummaryComponent
    }
];
