import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { FundSummaryComponent } from 'app/modules/admin/fund-summary/fund-summary.component';
import { fundRoutes } from 'app/modules/admin/fund-summary/fund-summary.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';

import * as $ from 'jquery';
window['$'] = window['jquery'] = $;

//declare var $: any;

@NgModule({
    declarations: [
        FundSummaryComponent
    ],
    imports     : [
        RouterModule.forChild(fundRoutes),
        MatButtonModule,
        MatIconModule,
        MatInputModule,        
        SharedModule,
        MatChipsModule,
        MatDividerModule,
        MatFormFieldModule,
        MatMenuModule,
        MatMomentDateModule,
        MatSelectModule,
        MatTooltipModule,
        SharedModule,
        DataTablesModule,
        HttpClientModule
    ]
})
export class FundSummaryModule
{
}
