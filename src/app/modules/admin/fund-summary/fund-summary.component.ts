import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { CommonComponent } from '../../common/common.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as $ from 'jquery';




@Component({
  selector: 'app-fund-summary',
  templateUrl: './fund-summary.component.html',
  styleUrls: ['./fund-summary.component.scss']
})

export class FundSummaryComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  showProceed = false;

  constructor(private service: SharedService,
    private _Activatedroute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private modalService: NgbModal,
    private snackBar: MatSnackBar
  ) {
    this.ISError = false;
  }

  isShown: boolean;
  searchForm: FormGroup;
  sBroker: string;
  sCIK: string;
  FundSummaryList: any = [];
  FundSummaryListData: any = [];
  AdditionalData: any = [];
  SelectedItems: any = [];
  SelectAllItems: any = [];
  FundBrokerListNew: any = [];
  ItemsIDsList: any = [];
  common: any = [];
  gridForm: FormGroup;
  ISError: boolean;
  showResults = false;
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 50,
      processing: true,
      destroy: true,
      dom: 'lBfrtip',
      order: [[0, 'asc']],
      lengthMenu: [
        [50, 100, 200, -1],
        ['50 rows', '100 rows', '200 rows', 'Show all']
      ],
      buttons: [
        'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
      ]
    };
    this.common = new CommonComponent();

    this.searchForm = this._formBuilder.group({
      sBroker: [''],
      sCIK: ['']
    });

    this.sBroker = sessionStorage.getItem('sBroker');
    this.sCIK = sessionStorage.getItem('sCIK');
    if (this.sBroker == null || this.sBroker == '') { this.sBroker = '0'; }
    if (this.sCIK == null || this.sCIK == '') { this.sCIK = '0'; }
    if (this.sCIK != '0' || this.sBroker != '0') { this.isShown = true; }
    else { this.isShown = false; }

    var data1 = {
      sBroker: this.sBroker,
      sCIK: this.sCIK,
    }

    this.searchForm.patchValue(
      data1
    );


    this.GetBrokersList();

    this.SelectedItems = [];
    this.SelectAllItems = [];



  }

  toggleShow() {
    this.isShown = !this.isShown;
  }

  searchFunds() {
    console.log('Search');
    this.sBroker = this.searchForm.controls.sBroker.value;
    this.sCIK = this.searchForm.controls.sCIK.value;

    if (this.sCIK == null || this.sCIK == '') { this.sCIK = '0'; }

    sessionStorage.setItem('sBroker', this.sBroker);
    sessionStorage.setItem('sCIK', this.sCIK);
    this.reLoad();
  }

  reLoad() {
    console.log("reload");
    if (this._router.url.substr(12, 1) == '1') {
      this._router.navigate([this._router.url.substr(0, 12) + '2' + this._router.url.substr(13, 100)])
    }
    else if (this._router.url.substr(12, 1) == '2') {
      this._router.navigate([this._router.url.substr(0, 12) + '1' + this._router.url.substr(13, 100)])
    }
    else {
      this._router.navigate([this._router.url.substr(0, 12) + '1' + this._router.url.substr(13, 100)])
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  public open(item) {
    const redirectURL = '/fundmaster/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public openinvestors(item) {
    const redirectURL = '/fundinvestorinfo/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public opennotices(item) {
    const redirectURL = '/fundnotice/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public opentasks(item: string) {
    sessionStorage.setItem('FM_ID', item);
    const redirectURL = '/tasks/';
    this._router.navigateByUrl(redirectURL);
  }

  public breadcrumb1() {
    const redirectURL = '/fundsummary'
    this._router.navigateByUrl(redirectURL);
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  refreshFundSummary() {
    console.log('refresh');
    this.FundSummaryList = [];
    this.showResults = false;
    this.common.showLoader()
    this.service.getFundSummaryList(this.sBroker, this.sCIK).subscribe(data => {
      this.rerender();
      if (Object.keys(data).length === 0) {
        console.log("no data")
        this.common.hideLoader();
      } else {
        this.FundSummaryListData = data['Data'];
        let count = 0
        this.FundSummaryListData.forEach(value => {
          this.ItemsIDsList.push(count);
          count++;
          let myNodeList =
          {
            FM_ID: value.FM_ID,
            FM_Name: value.FM_Name,
            Investors: value.Investors,
            Amount: value.Amount,
          }
          this.FundSummaryList.push(myNodeList);

        });
        this.showResults = true;
        //this.common.refreshView();

        //console.log(this.FundInvestorInfoList);
        this.common.hideLoader();


      }
    }
    )
  }

  GetBrokersList() {
    this.service.getBrokersList().subscribe((data: any[]) => {
      this.FundBrokerListNew = data;
      this.refreshFundSummary();
    }
    )
  }

}
