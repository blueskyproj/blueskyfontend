import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { Router } from '@angular/router';
import { ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'dashboard',
    templateUrl    : './dashboard.component.html',
    styleUrls      : ['./dashboard.component.scss'],
})

export class DashboardComponent implements OnInit {

  constructor(private service:SharedService, private _router: Router) { 
    this.ISError = false;
    sessionStorage.removeItem('FM_ID');
  }
  EventLoggerList:any=[];
  AdditionalData:any=[];
  FStop5List:any=[];
  Tasktop5Upcoming:any=[];
  Tasktop5Done:any=[];
  Tasktop5Expired:any=[];  

  ISError:boolean;
  ngOnInit(): void {
    this.refreshFSTop5List();
    this.refreshTaskTop5Upcoming();
    this.refreshTaskTop5Done();
    this.refreshTaskTop5Expired();
    this.refreshEventLoggerSummary();
  }


  public open(item) {
    console.log("open");
    const redirectURL = '/fundmaster/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public openinvestors(item) {
    const redirectURL = '/fundinvestorinfo/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public opennotices(item) {
    const redirectURL = '/fundnotice/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public opentasks(item :string) {
    sessionStorage.setItem('FM_ID', item);
    const redirectURL = '/tasks/';
    this._router.navigateByUrl(redirectURL);
  }

  refreshEventLoggerSummary() {
    this.service.getEventLoggerList().subscribe(data=>{
      this.EventLoggerList=data['Data'];
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Status==='Errors'){
        this.ISError = true;
      } else {
        this.ISError = false;
      }
    }
    )
  }

  refreshTaskTop5Upcoming() {
    this.service.getTasksTop5Upcoming().subscribe(data=>{
      this.Tasktop5Upcoming=data['Data'];
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Status==='Errors'){
        this.ISError = true;
      } else {
        this.ISError = false;
      }
    }
    )
  }

    refreshTaskTop5Done() {
    this.service.getTasksTop5Done().subscribe(data=>{
      this.Tasktop5Done=data['Data'];
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Status==='Errors'){
        this.ISError = true;
      } else {
        this.ISError = false;
      }
    }
    )
  }

  refreshTaskTop5Expired() {
    this.service.getTasksTop5Expired().subscribe(data=>{
      this.Tasktop5Expired=data['Data'];
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Status==='Errors'){
        this.ISError = true;
      } else {
        this.ISError = false;
      }
    }
    )
  }

  refreshFSTop5List() {
    console.log("top5fund");
    this.service.getFundSummaryTop5().subscribe(data=>{
      this.FStop5List=data['Data'];
      //console.log(this.FundSummaryList);
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Status==='Errors'){
        this.ISError = true;
      } else {
        this.ISError = false;
      }
    }
    )
  }


}

