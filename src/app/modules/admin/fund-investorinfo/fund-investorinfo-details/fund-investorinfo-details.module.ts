import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'app/shared/shared.module';
import { FundInvestorinfoDetailsRoutes } from 'app/modules/admin/fund-investorinfo/fund-investorinfo-details/fund-investorinfo-details-routing.module';
import { MatStepperModule } from '@angular/material/stepper';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CommonModule } from '@angular/common';
import { FundInvestorinfoDetailsComponent } from './fund-investorinfo-details.component';


@NgModule({
  declarations: [
    FundInvestorinfoDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(FundInvestorinfoDetailsRoutes),
    MatButtonModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatMomentDateModule,
    MatSelectModule,
    SharedModule,
    MatStepperModule,
    ImageCropperModule    
  ]
})
export class FundInvestorinfoDetailsModule { }

