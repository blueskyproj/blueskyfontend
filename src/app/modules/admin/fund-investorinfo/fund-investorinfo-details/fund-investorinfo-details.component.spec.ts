import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundInvestorinfoDetailsComponent } from './fund-investorinfo-details.component';

describe('FundInvestorinfoDetailsComponent', () => {
  let component: FundInvestorinfoDetailsComponent;
  let fixture: ComponentFixture<FundInvestorinfoDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundInvestorinfoDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundInvestorinfoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
