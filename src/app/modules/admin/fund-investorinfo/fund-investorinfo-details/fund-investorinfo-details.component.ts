import { SharedService } from 'app/shared.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Directive, Input, NgModule, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import $ from "jquery";
import { CommonComponent } from '../../../common/common.component';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpClient } from '@angular/common/http';
import { formatNumber } from '@angular/common';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-fund-investorinfo-details',
  templateUrl: './fund-investorinfo-details.component.html',
  styleUrls: ['./fund-investorinfo-details.component.scss']
})
export class FundInvestorinfoDetailsComponent implements OnInit {

  fundinvestorinfoForm: FormGroup;
  ISError: boolean;

  constructor(private service: SharedService,
    private _formBuilder: FormBuilder,
    private _Activatedroute: ActivatedRoute,
    private _router: Router,
    private http: HttpClient
  ) {
    this.ISError = false;
  }

  common: any = [];
  FI_Name: string;
  FI_Country: string;
  FI_SubsAmnt: string;
  FI_CommitDate: string;
  FI_New: string;
  FI_Jur: string;

  FI_ID: string;
  FI_FM_ID: string;;
  FINewList: any = [{ "ID": 1, "FINew": "New" }, { "ID": 2, "FINew": "Existing" }];
  NewFundInvestorInfoList: any = [];
  FundInvestorInfoList: any = [];
  FundInvestorInfoListData: any = [];
  CountryList: any = [];
  StatesList: any = [];
  AdditionalData: any = [];
  SelectedItems: any = [];
  SelectAllItems: any = [];
  ItemsIDsList: any = [];


  ngOnInit(): void {
    this.common = new CommonComponent();
    this.FI_ID = this._Activatedroute.snapshot.paramMap.get("id");
    this.FI_FM_ID = sessionStorage.getItem('FM_ID');
    this.getFundInvestorInfoDetails(this.FI_ID);
    this.fundinvestorinfoForm = this._formBuilder.group({
      FI_ID: [''],
      FI_FM_ID: [''],
      FI_Name: [''],
      FI_Jur: [''],
      FI_Country: [''],
      FI_SubsAmnt: [''],
      FI_CommitDate: [''],
      FI_New: [''],
    });
  }

  getFundInvestorInfoDetails(iFI_ID: any) {
    this.service.getFundInvestorInfoDetailsID(iFI_ID).subscribe((data: any[]) => {
      this.FundInvestorInfoList = data;
      this.onLoadData();
    }
    )
  }

  onLoadData() {


    var data1 = {
      FI_Name: this.FundInvestorInfoList[0].FI_Name,
      FI_Country: this.FundInvestorInfoList[0].FI_Country,
      FI_Jur: this.FundInvestorInfoList[0].FI_Jur,
      FI_SubsAmnt: formatNumber(this.FundInvestorInfoList[0].FI_SubsAmnt, 'en-US', '1.2-2'),
      FI_CommitDate: this.FundInvestorInfoList[0].FI_CommitDate.split('/').join('-').substring(0, 10),
      FI_New: this.FundInvestorInfoList[0].FI_New,
      //FI_FM_ID: this.FundInvestorInfoList[0].FI_FM_ID,
      FI_FM_ID: sessionStorage.getItem('FM_ID'),
      FI_ID: this.FundInvestorInfoList[0].FI_ID
    }

    this.fundinvestorinfoForm.patchValue(
      data1
    );

    this.GetStatesList();
  }

  GetStatesList() {
    this.service.getStatesList().subscribe((data: any[]) => {
      this.StatesList = data;
      this.GetCountryList();

    }
    )
  }

  GetCountryList() {
    this.service.getCountryList().subscribe((data: any[]) => {
      this.CountryList = data;
    }
    )
  }


  updateFundInvestorInfo() {
    var val = {

      FI_ID: this.fundinvestorinfoForm.controls.FI_ID.value,
      FI_FM_ID: this.fundinvestorinfoForm.controls.FI_FM_ID.value,
      FI_Name: this.fundinvestorinfoForm.controls.FI_Name.value,
      FI_Country: this.fundinvestorinfoForm.controls.FI_Country.value,
      FI_Jur: this.fundinvestorinfoForm.controls.FI_Jur.value,
      FI_SubsAmnt: this.fundinvestorinfoForm.controls.FI_SubsAmnt.value.replace(',', ''),
      FI_CommitDate: this.fundinvestorinfoForm.controls.FI_CommitDate.value,
      FI_New: this.fundinvestorinfoForm.controls.FI_New.value

    };
    var val2 = { U_ID: sessionStorage.getItem('U_ID') }
    //send
    var valcomp = {

      FundInvestorInfo: val,
      User: val2
    };
    this.service.updateFundInvestorInfo(valcomp).subscribe(res => {

    });
    this.openinvestorinfos();
  }

  public openinvestorinfos() {
    const redirectURL = '/fundinvestorinfo/' + this.FI_FM_ID;
    this._router.navigateByUrl(redirectURL);
  }

  deleteRecord() {
    console.log('delete');
    this.service.deleteFundInvestorInfo(this.fundinvestorinfoForm.controls.FI_ID.value).subscribe(res => {
    this.openinvestorinfos();

    });
  }

}