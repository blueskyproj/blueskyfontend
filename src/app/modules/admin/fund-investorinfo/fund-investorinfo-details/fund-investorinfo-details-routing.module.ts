import { Route, RouterModule, Routes } from '@angular/router';
import { FundInvestorinfoDetailsComponent } from 'app/modules/admin/fund-investorinfo/fund-investorinfo-details/fund-investorinfo-details.component';
export const FundInvestorinfoDetailsRoutes: Route[] = [
    {
        path     : '',
        component: FundInvestorinfoDetailsComponent
    }
];
