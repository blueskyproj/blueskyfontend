import { Component, Input, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { Router} from '@angular/router';

@Component({
  selector: 'app-file-upload-modal',
  templateUrl: './file-upload-modal.component.html',
  styleUrls: ['./file-upload-modal.component.scss']
})

export class FileUploadModalComponent implements OnInit {
  @Input() id: string;
  sMessage: string;
  messageList: any = [];
  sIcon: string;

  myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });

  constructor(public modal: NgbActiveModal, private http: HttpClient , private router: Router) { }

  ngOnInit(): void {
  }

  uploadFile() {
    this.sMessage = "Hello";
    console.log(this.sMessage);

    Swal.fire({
      title: 'Success',
      text: this.sMessage,
      icon: 'success'
    });
    this.modal.dismiss();
  }

  get f() {
    return this.myForm.controls;
  }

  onFileChange(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.myForm.patchValue({
        fileSource: file
      });
    }
  }

  submit() {


    
    const formData = new FormData();
    formData.append('file', this.myForm.get('fileSource').value);
    formData.append('userid', sessionStorage.getItem('U_ID'));
    formData.append('name', "");
    formData.append('fm_id', this.id);
    this.http.post(environment.configurationsURL + '/InvestorFileUpload/FileUploads', formData, {responseType: 'text' }).subscribe(res => {

      this.sMessage =res
      console.log("sMessage" + this.sMessage);
      if (this.sMessage == "File Uploaded Successfully") {
        this.sIcon = "success";
      } else {
        this.sIcon = "info";
      }

      Swal.fire({
        title: 'Record Submitted',
        text: this.sMessage,
        icon: this.sIcon
      });
      
    });
    
     this.modal.dismiss();
     let currentUrl = this.router.url;
    console.log(currentUrl);
    //console.log("messageList" + this.messageList);

     this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
         this.router.navigate([currentUrl]);
     });
     
  }
}
