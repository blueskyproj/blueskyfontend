import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundInvestorInfoComponent } from './fund-investorinfo.component';

describe('FundInvestorInfoComponent', () => {
  let component: FundInvestorInfoComponent;
  let fixture: ComponentFixture<FundInvestorInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundInvestorInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundInvestorInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
