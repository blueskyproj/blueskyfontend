import { Route } from '@angular/router';
import { FundInvestorInfoComponent } from 'app/modules/admin/fund-investorinfo/fund-investorinfo.component';

export const fundRoutes: Route[] = [
    {
        path     : '',
        component: FundInvestorInfoComponent
    }
];
