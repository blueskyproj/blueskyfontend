//import $ from "jquery";
//import $ from '../../../../../node_modules/jquery/dist/jquery.min.js';



import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { CommonComponent } from '../../common/common.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModalComponent } from './file-upload-modal/file-upload-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-fund-investorinfo',
  templateUrl: './fund-investorinfo.component.html',
  styleUrls: ['./fund-investorinfo.component.scss']
})

export class FundInvestorInfoComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  showProceed = false;

  constructor(private service: SharedService,
    private _Activatedroute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private modalService: NgbModal,
    private snackBar: MatSnackBar
  ) {
    this.ISError = false;
  }
  common: any = [];
  gridForm: FormGroup;
  iFMID: number;
  FINewList: any = [{ "ID": 1, "FINew": "New" }, { "ID": 2, "FINew": "Existing" }];
  NewFundInvestorInfoList: any = [];
  FundInvestorInfoList: any = [];
  FundInvestorInfoListData: any = [];
  CountryList: any = [];
  StatesList: any = [];
  AdditionalData: any = [];
  SelectedItems: any = [];
  SelectAllItems: any = [];
  ItemsIDsList: any = [];
  showResults = false;
  FundSummaryList: any = [];

  ISError: boolean;
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 50,
      processing: true,
      destroy: true,
      //dom: 'Bfrltip',
      dom: 'lBfrtip',
      order: [[2, 'asc']],
      lengthMenu: [
        [50, 100, 200, -1],
        ['50 rows', '100 rows', '200 rows', 'Show all']
      ],      
      buttons: [
        'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
      ],
      columns: [ { "searchable": true }, { "searchable": false }, { "searchable": false }, { "searchable": false }, { "searchable": false }, { "searchable": false } ],
    };
    this.common = new CommonComponent();
    this.iFMID = Number(this._Activatedroute.snapshot.paramMap.get("id"));
    // this.GetStatesList();
    this.refreshFundInvestorInfo();
    this.SelectedItems = [];
    this.SelectAllItems = [];
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  public open(item) {
    const redirectURL = '/fundmaster/' + item
    this._router.navigateByUrl(redirectURL);
  }


  public opennotices() {
    const redirectURL = '/fundnotice/' + this.iFMID
    this._router.navigateByUrl(redirectURL);
  }

  showFileUploadModal() {
    const modalRef = this.modalService.open(FileUploadModalComponent, { size: 'md' });
    modalRef.componentInstance.id = this.iFMID;
  }

  public opentasks() {
    sessionStorage.setItem('FM_ID', this.iFMID.toString());
    const redirectURL = '/tasks/';
    this._router.navigateByUrl(redirectURL);
  }


  public breadcrumb1() {
    const redirectURL = '/fundsummary'
    this._router.navigateByUrl(redirectURL);
  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  refreshFundInvestorInfo() {

    this.FundInvestorInfoList = [];
    this.showResults = false;
    this.common.showLoader()
    this.service.getFundInvestorInfoID(this.iFMID).subscribe(data => {
      //this.dtTrigger.next()
      this.rerender();
      if (Object.keys(data).length === 0) {
        console.log("no data")
      } else {
        this.FundInvestorInfoListData = data['Data'];
        let count = 0
        this.FundInvestorInfoListData.forEach(value => {
          let SortedFundInvestorInfoListCountry = [];
          let SortedFundInvestorInfoListStates = [];
          let SortedFundInvestorInfoListNew = [];
          let sNew = "";
          this.ItemsIDsList.push(count);
          count++;

          this.FINewList.forEach(val => {
            if (val.ID === Number(value.FI_New)) {
              sNew = val.FINew;
            }
          });

          let myNodeList =
          {
            FI_ID: value.FI_ID,
            FI_Name: value.FI_Name,
            FundInvestorInfoSelectCountry: SortedFundInvestorInfoListCountry,
            FundInvestorInfoSelectNew: SortedFundInvestorInfoListNew,
            FundInvestorInfoSelectStates: SortedFundInvestorInfoListStates,
            FI_SubsAmnt: value.FI_SubsAmnt,
            FM_Currency: value.FM_Currency,
            FI_CommitDate: value.FI_CommitDate,
            FI_New: value.FI_New,
            C_Name: value.C_Name,
            S_Name: value.S_Name,
            sNew: sNew

          }
          this.FundInvestorInfoList.push(myNodeList);

        });
        this.showResults = true;
        //this.common.refreshView();
        console.log('refresh');
        //console.log(this.FundInvestorInfoList);
        this.common.hideLoader();


      }
    }
    )
  }

  public newFundInvestorInfo(ID) {
    sessionStorage.setItem('FM_ID', this.iFMID.toString());
    const redirectURL = '/fundinvestorinfo/fundinvestorinfodetails/' + ID;
    this._router.navigateByUrl(redirectURL);
  }


}
