export interface Notice
{
	FN_ID: number;
	FN_FM_ID: number;
	FN_State: number;
	FN_Number: number;
	FN_Type_Filing: number;
	FN_FilingDate: string;
	FN_Exemption: string;
	FN_FilingDoc: string;
	FN_StateCorresDocID: string;
	FN_Notes: string;
    S_Name: string;
    FT_Desc: string;
	editNotice: string;

}

