import { Route } from '@angular/router';
import { FundNoticeComponent } from 'app/modules/admin/fund-notice/fund-notice.component';

export const fundRoutes: Route[] = [
    {
        path     : '',
        component: FundNoticeComponent
    }
];
