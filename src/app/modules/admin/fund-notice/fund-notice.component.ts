import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { CommonComponent } from '../../common/common.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import * as $ from 'jquery';
import {Router, NavigationEnd,ActivatedRoute} from '@angular/router';


@Component({
  selector: 'contacts-list',
  templateUrl: './fund-notice.component.html',
  styleUrls: ['./fund-notice.component.scss']
})

export class FundNoticeComponent implements OnInit {

   

  //dtOptions: DataTables.Settings = {};
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  showProceed = false;

  constructor(private service: SharedService,
    private _Activatedroute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private modalService: NgbModal,
    private snackBar: MatSnackBar
  ) {
    this.ISError = false;
    this._router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

  }

  common: any = [];
  iFMID: number;
  sFMID: string;
  FundSummaryList: any = [];
  FundInvestorNoticeList: any = [];
  isInvestor: boolean;
  isLatest: boolean;
  sInvestor: string;
  sLatest: string; 

  ISError: boolean;
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [],
      processing: true,
      dom: 'lBfrtip',
      lengthMenu: [
        [10, 25, 50, -1],
        ['10 rows', '25 rows', '50 rows', 'Show all']
      ],
      buttons: [
        'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
      ]
    };
    this.common = new CommonComponent();
    this.iFMID = Number(this._Activatedroute.snapshot.paramMap.get("id"));
    this.sFMID = this._Activatedroute.snapshot.paramMap.get("id");

    this.sInvestor = sessionStorage.getItem('sInvestors');
    this.sLatest = sessionStorage.getItem('sLatest');

    if (this.sInvestor == null) { this.sInvestor = '0'; } 
    if (this.sLatest == null) { this.sLatest = '0'; }

    if (this.sInvestor === '0') { this.isInvestor = false; } else { this.isInvestor = true; }
    if (this.sLatest === '0') { this.isLatest = false; } else { this.isLatest = true; }

    this.refreshFundNotice();

  }


  public open(item) {
    const redirectURL = '/fundmaster/' + item
    this._router.navigateByUrl(redirectURL);
  }

  public openinvestors() {
    const redirectURL = '/fundinvestorinfo/' + this.iFMID
    this._router.navigateByUrl(redirectURL);
  }

  public newNoticeDetails(ID) {
    sessionStorage.setItem('FM_ID', this.sFMID);
    const redirectURL = '/fundnotice/fndetails/' + ID;
    this._router.navigateByUrl(redirectURL);
  }


  public opentasks() {
    sessionStorage.setItem('FM_ID', this.iFMID.toString());
    const redirectURL = '/tasks/';
    this._router.navigateByUrl(redirectURL);
  }


  public breadcrumb1() {
    const redirectURL = '/fundsummary'
    this._router.navigateByUrl(redirectURL);
  }

  refreshFundNotice() {
    console.log("refreshFundNotice")
    // Get the notices
    this.FundInvestorNoticeList = [];

    // this.showResults = false;
    // this.common.showLoader()
    this.service.getFundNoticeID(this.iFMID, this.sInvestor, this.sLatest).subscribe((data: any[]) => {
      this.FundInvestorNoticeList = data;
      this.dtTrigger.next()
    }


    )
  }

  applyInvestors(e) {

    if (e.target.checked) {
      sessionStorage.setItem('sInvestors', '1');
    }
    else
    {
      sessionStorage.setItem('sInvestors', '0');
    }
    this.reLoad();
  }
  


  applyLatest(e) {
    
    if (e.target.checked) {
      sessionStorage.setItem('sLatest', '1');
    }
    else
    {
      sessionStorage.setItem('sLatest', '0');
    }
    this.reLoad();
  }

  reLoad(){
    console.log("reload");
    if (this._router.url.substr(11,1) == '1' ) 
    {
      this._router.navigate([this._router.url.substr(0,11) + '2' + this._router.url.substr(12,100)])
    }
    else if (this._router.url.substr(11,1) == '2' ) 
    {
      this._router.navigate([this._router.url.substr(0,11) + '1' + this._router.url.substr(12,100)])
    }
    else
    {
      this._router.navigate([this._router.url.substr(0,11) + '1' + this._router.url.substr(11,100)])
    }
 }

}
