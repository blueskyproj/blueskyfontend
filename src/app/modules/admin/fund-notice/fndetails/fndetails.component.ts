import { SharedService } from 'app/shared.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Directive, Input, NgModule, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import $ from "jquery";
import { CommonComponent } from '../../../common/common.component';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-fndetails',
  templateUrl: './fndetails.component.html',
  styleUrls: ['./fndetails.component.scss']
})
export class FndetailsComponent implements OnInit {

  noticeForm: FormGroup;
  ISError: boolean;

  constructor(private service: SharedService,
    private _formBuilder: FormBuilder,
    private _Activatedroute: ActivatedRoute,
    private _router: Router,
    private http: HttpClient
  ) {
    this.ISError = false;
  }

  StatesList: any = [];
  FilingTypeList: any = [];
  NoticeList: any = [];
  FN_ID: string;
  FN_FM_ID: string;
  FN_State: string;
  FN_Number: string;
  FN_Type_Filing: string;
  FN_FilingDate: string;
  FN_Exemption: string;
  FN_FilingDoc: string;
  FN_StateCorresDocID: string;
  FN_Notes: string;

  


  ngOnInit(): void {
    this.FN_ID = this._Activatedroute.snapshot.paramMap.get("id");
    this.FN_FM_ID = sessionStorage.getItem('FM_ID');
    this.getFnDetails(this.FN_ID);
    this.noticeForm = this._formBuilder.group({
      FN_ID: [''],
      FN_FM_ID: [''],
      FN_State: [''],
      FN_Type_Filing: [''],
      FN_FilingDate: [''], 
      FN_FilingDoc: [''],
      FN_Exemption: [''],
      FN_StateCorresDocID: [''],
      FN_Notes: ['']
    });


  }



  getFnDetails(iFN_ID: any) {
    this.service.getFnDetailsID(iFN_ID).subscribe((data: any[]) => {
      this.NoticeList = data;
      this.onLoadData();
    }
    )
  }

  onLoadData() {

    var data1 = {
      FN_State: this.NoticeList[0].FN_State,
      FN_Type_Filing: this.NoticeList[0].FN_Type_Filing,
      FN_FilingDoc: this.NoticeList[0].FN_FilingDoc,
      FN_StateCorresDocID: this.NoticeList[0].FN_StateCorresDocID,
      FN_FilingDate: this.NoticeList[0].FN_FilingDate.split('/').join('-').substring(0, 10),
      FN_Notes: this.NoticeList[0].FN_Notes,
      FN_Exemption: this.NoticeList[0].FN_Exemption,
      FN_ID: this.NoticeList[0].FN_ID,
      FN_FM_ID: sessionStorage.getItem('FM_ID')
    }

    this.noticeForm.patchValue(
      data1
    );

    this.GetStatesList();
  }

  GetStatesList() {
    this.service.getStatesList().subscribe((data: any[]) => {
      this.StatesList = data;
      this.GetFilingTypeList();
    }
    )
  }

  GetFilingTypeList() {
    this.service.getFilingTypeList().subscribe((data: any[]) => {
      this.FilingTypeList = data;
    }
    )
  }

  updateFundNotice() {
    var val = {

      FN_ID: this.noticeForm.controls.FN_ID.value,
      FN_FM_ID: this.noticeForm.controls.FN_FM_ID.value,
      FN_State: this.noticeForm.controls.FN_State.value,
      FN_Type_Filing: this.noticeForm.controls.FN_Type_Filing.value,
      FN_FilingDate: this.noticeForm.controls.FN_FilingDate.value,
      FN_Exemption: this.noticeForm.controls.FN_Exemption.value,
      FN_FilingDoc: this.noticeForm.controls.FN_FilingDoc.value,
      FN_StateCorresDocID: this.noticeForm.controls.FN_StateCorresDocID.value,
      FN_Notes: this.noticeForm.controls.FN_Notes.value

    };
    var val2 = { U_ID: sessionStorage.getItem('U_ID') }
    //send
    var valcomp = {

      FundNotice: val,
      User: val2
    };
    console.log('update');
    this.service.updateFundNotice(valcomp).subscribe(res => {

    });
    this.opennotices();
  }

  public opennotices() {

    const redirectURL = '/fundnotice/' + this.FN_FM_ID;
    this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this._router.navigate([redirectURL]);
    });
  }


  deleteRecord() {
    var val = {
      FN_ID: this.noticeForm.controls.FN_ID.value,
    };
    this.service.deleteFundNotice(val).subscribe(res => {
      this.opennotices();
    });
  }

}