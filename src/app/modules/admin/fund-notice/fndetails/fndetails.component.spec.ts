import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FndetailsComponent } from './fndetails.component';

describe('FndetailsComponent', () => {
  let component: FndetailsComponent;
  let fixture: ComponentFixture<FndetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FndetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FndetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
