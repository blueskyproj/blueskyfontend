import { Route, RouterModule, Routes } from '@angular/router';
import { FndetailsComponent } from 'app/modules/admin/fund-notice/fndetails/fndetails.component';

export const FndetailsRoutes: Route[] = [
    {
        path     : '',
        component: FndetailsComponent
    }
];
