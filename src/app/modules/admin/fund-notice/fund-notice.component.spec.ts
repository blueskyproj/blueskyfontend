import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundNoticeComponent } from './fund-notice.component';

describe('FundNoticeComponent', () => {
  let component: FundNoticeComponent;
  let fixture: ComponentFixture<FundNoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundNoticeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
