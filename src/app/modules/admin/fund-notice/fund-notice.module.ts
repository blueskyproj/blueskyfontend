import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { FundNoticeComponent } from 'app/modules/admin/fund-notice/fund-notice.component';
import { fundRoutes } from 'app/modules/admin/fund-notice/fund-notice.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';

import * as $ from 'jquery';
window['$'] = window['jquery'] = $;

//declare var $: any;

@NgModule({
    declarations: [
        FundNoticeComponent,
        
    ],
    imports     : [
        RouterModule.forChild(fundRoutes),
        MatButtonModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatMomentDateModule,
        MatSelectModule,
        MatTooltipModule,
        SharedModule,
        DataTablesModule,
        HttpClientModule
        
    ]
})

export class FundNoticeModule
{
}

