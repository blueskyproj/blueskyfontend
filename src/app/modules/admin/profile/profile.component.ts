import { SharedService } from 'app/shared.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Directive, Input, NgModule, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import $ from "jquery";
import { CommonComponent } from '../../common/common.component';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { user } from 'app/data/mock/common/user/data';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ProfileComponent implements OnInit {

  imgChangeEvt: any = '';
  cropImgPreview: any = '';
  croppedImg: any = '';
  fileToUpload: File = null;

  myForm = new FormGroup({
    filecropImgPreview: new FormControl(''),
  });
  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(private service: SharedService,
    private _formBuilder: FormBuilder,
    private _Activatedroute: ActivatedRoute,
    private _router: Router,
    private http: HttpClient
  ) {
    this.ISError = false;
  }

  UsersList: any = [];
  verticalStepperForm: FormGroup;

  iUID: number;
  U_FirstName: string;
  U_LastName: string;
  U_Title: string;
  U_Email: string;
  U_Username: string;
  U_Phone: string;
  U_Password: string;
  ISError: boolean;
  stitle: string;
  stext: string;
  sicon: string;

  ngOnInit(): void {


    this.iUID = Number(sessionStorage.getItem('U_ID'));
    this.GetUsersID(this.iUID);
    this.verticalStepperForm = this._formBuilder.group({
      step1: this._formBuilder.group({
        U_Username: ['', Validators.required],
        U_FirstName: ['', Validators.required],
        U_LastName: ['', Validators.required],
        U_Title: [''],
        U_Gender: [''],
        U_Email: ['', Validators.required],
        U_Phone: ['']
      }),
    }
    );


  }

  updateUser() {
    var val = {
      U_FirstName: this.U_FirstName,
      U_LastName: this.U_LastName,
      U_Title: this.U_Title,
      U_Email: this.U_Email,
      U_Phone: this.U_Phone,
      U_Password: this.U_Password,
      U_Username: this.U_Username,
      U_ID: this.iUID
    };
    this.service.updateUser(val).subscribe(res => {
      alert(res.toString());
    });
  }

  GetUsersID(iUID: any) {
    this.service.getUsersID(iUID).subscribe((data: any[]) => {
      this.UsersList = data;
      this.onLoadData();
    }
    )
  }

  onLoadData() {
    var data1 = {
      step1:
      {
        U_Username: this.UsersList[0].U_Username,
        U_FirstName: this.UsersList[0].U_FirstName,
        U_LastName: this.UsersList[0].U_LastName,
        U_Title: this.UsersList[0].U_Title,
        U_Gender: this.UsersList[0].U_Gender,
        U_Email: this.UsersList[0].U_Email,
        U_Phone: this.UsersList[0].U_Phone
      }
    }
    this.verticalStepperForm.patchValue(
      data1
    );

  }

  onFileChange(event: any): void {
    this.imgChangeEvt = event;
  }
  cropImg(e: ImageCroppedEvent) {
    this.cropImgPreview = e.base64;
    let File = base64ToFile(this.cropImgPreview);
    const base64img = e.base64;
  }

  imgLoad() {
    // display cropper tool
  }

  submit(e: ImageCroppedEvent) {

    this.croppedImg = document.getElementById("filecropImgPreview").getAttribute('src');

    const formData = new FormData();
    formData.append('file', this.croppedImg);
    formData.append('userid', sessionStorage.getItem('U_ID'));

    this.http.post(environment.configurationsURL + '/UserProfileImgUpload/FileUploads/', formData)

      .subscribe(res => {

      })
    this.updateImage()

    this._router.navigate(['/scripts/client/profile']);
    Swal.fire({
      title: 'Success',
      text: "Image uploaded successfully!!",
      icon: 'success'
    });


  }

  updateImage() {
    document.getElementById('avatar-img').setAttribute('src', '../../../avatars/avatar_' + sessionStorage.getItem('U_ID') + '.png?' + new Date().getTime());
  }


  initCropper() {
    // init cropper
  }

  imgFailed() {
    // error msg
  }

}
