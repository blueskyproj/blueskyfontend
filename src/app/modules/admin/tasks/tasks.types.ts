export interface Tag
{
    id?: string;
    title?: string;
}

export interface Task
{
    id: string;
    section: number,
    title: string;
    notes: string;
    priority: 0 | 1 | 2;
    fund: number;
    dueDate: string | null;
    doneDate: string | null;
    doneBy: number;
    type: 'task' | 'section';
    allocated: number;
    completed: boolean;
    tags: string[];
    order: number;
}

