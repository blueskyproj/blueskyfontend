import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Tag, Task } from 'app/modules/admin/tasks/tasks.types';
import { SharedService } from 'app/shared.service';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TasksService
{
    // Private
    private _tags: BehaviorSubject<Tag[] | null>;
    private _task: BehaviorSubject<Task | null>;
    private _tasks: BehaviorSubject<Task[] | null>;
    sFM_id: string;
    newRecordId:number = 0;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private service: SharedService,
    )
    {
        // Set the private defaults
        this._tags = new BehaviorSubject(null);
        this._task = new BehaviorSubject(null);
        this._tasks = new BehaviorSubject(null);
    }

    readonly API_URL = environment.apiUrl;
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for tags
     */
    get tags$(): Observable<Tag[]>
    {
        return this._tags.asObservable();
    }

    /**
     * Getter for task
     */
    get task$(): Observable<Task>
    {
        return this._task.asObservable();
    }

    /**
     * Getter for tasks
     */
    get tasks$(): Observable<Task[]>
    {
        return this._tasks.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tags
     */
    getTags(): Observable<Tag[]>
    {
        return this._httpClient.get<Tag[]>('api/apps/tasks/tags').pipe(
            tap((response: any) => {
                this._tags.next(response);
            })
        );
    }

    /**
     * Crate tag
     *
     * @param tag
     */
    createTag(tag: Tag): Observable<Tag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.post<Tag>('api/apps/tasks/tag', {tag}).pipe(
                map((newTag) => {

                    // Update the tags with the new tag
                    this._tags.next([...tags, newTag]);

                    // Return new tag from observable
                    return newTag;
                })
            ))
        );
    }

    /**
     * Update the tag
     *
     * @param id
     * @param tag
     */
    updateTag(id: string, tag: Tag): Observable<Tag>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.patch<Tag>('api/apps/tasks/tag', {
                id,
                tag
            }).pipe(
                map((updatedTag) => {

                    // Find the index of the updated tag
                    const index = tags.findIndex(item => item.id === id);

                    // Update the tag
                    tags[index] = updatedTag;

                    // Update the tags
                    this._tags.next(tags);

                    // Return the updated tag
                    return updatedTag;
                })
            ))
        );
    }

    /**
     * Delete the tag
     *
     * @param id
     */
    deleteTag(id: string): Observable<boolean>
    {
        return this.tags$.pipe(
            take(1),
            switchMap(tags => this._httpClient.delete('api/apps/tasks/tag', {params: {id}}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted tag
                    const index = tags.findIndex(item => item.id === id);

                    // Delete the tag
                    tags.splice(index, 1);

                    // Update the tags
                    this._tags.next(tags);

                    // Return the deleted status
                    return isDeleted;
                }),
                filter(isDeleted => isDeleted),
                switchMap(isDeleted => this.tasks$.pipe(
                    take(1),
                    map((tasks) => {

                        // Iterate through the tasks
                        tasks.forEach((task) => {

                            const tagIndex = task.tags.findIndex(tag => tag === id);

                            // If the task has a tag, remove it
                            if ( tagIndex > -1 )
                            {
                                task.tags.splice(tagIndex, 1);
                            }
                        });

                        // Return the deleted status
                        return isDeleted;
                    })
                ))
            ))
        );
    }

    /**
     * Get tasks
     */
     TasksList: any = [];
     TaskSectionsList: any = [];
     FullTasksList: any = [];
     TasksListResponse: any = [];

     GetTasksList(U_ID) { 

        this.service.getTasksList(U_ID).subscribe((data: any[]) => {
          this.TasksList = data;

          //console.log('Tasklist ' + this.TasksList)

        }
        )
      }

    getTasks(): Observable<Task[]>
    {
        console.log('getTasks');
        this.sFM_id = sessionStorage.getItem('FM_ID');
        if(this.sFM_id==null){
            this.sFM_id = '0';
        }
        let UnsortedResponse = [];
        let SortedResponse = [];
        return this._httpClient.get<Task[]>(this.API_URL+'/Tasks/'+ this.sFM_id ).pipe(
        //return this._httpClient.get<Task[]>('api/apps/tasks/all').pipe(
          tap((response) => {
              /*  Map respinse to model */
               UnsortedResponse = response;
               let count = 0;
               UnsortedResponse.forEach(value => {   
                  count++;
                  let comp =false;
                  if(Number(value.Tsk_Status)===0){
                      comp=false;
                  }
                  else{
                      comp=true;
                  }

                  let myNode = 
                  {        
                    id: Number(value.Tsk_ID),
                    section: Number(value.Tsk_Section),
                    title: value.Tsk_Title,
                    notes: value.Tsk_Text,
                    priority: Number(value.Tsk_Priority),
                    fund: Number(value.Tsk_FN_ID),
                    dueDate: value.Tsk_Deadline,
                    doneDate: value.Tsk_Date_Done,
                    doneBy: value.Tsk_U_ID_done ,
                    type: 'task',
                    allocated: Number(value.Tsk_U_ID_allocated),
                    completed: Number(value.Tsk_U_ID_done),
                    tags:  [ '2b884143-419a-45ca-a7f6-48f99f4e7798'],
                    order: count 
                  }
                  SortedResponse.push(myNode);    
                })
                let total = SortedResponse.length;
                let newtaskid =Number(SortedResponse[total-1].id)+1;
                this.newRecordId = newtaskid;
                let date: Date = new Date();  
               this.TasksListResponse = SortedResponse.sort( (a,b)=> a.id > b.id ? -1:1 )
               this._tasks.next(this.TasksListResponse);
           })
        );

        
        // this.service.getTasksList(sessionStorage.getItem('U_ID')).subscribe((data: any[]) => {
        //    this.TasksList = data;
        // })
        // //console.log(this.TasksList);

        // this.TasksList = this.GetTasksList(sessionStorage.getItem('U_ID')) ;

        // this.service.getTaskSectionsList(sessionStorage.getItem('U_ID')).subscribe(data => {
        //     if (Object.keys(data).length === 0) {
        //       console.log("no data")
        //     } else {
        //       this.TaskSectionsList = data['Data'];
        //       let count = 0

        //       for (var key in this.TaskSectionsList) {
                  
        //           if (this.TaskSectionsList.hasOwnProperty(key)) {

        //               //console.log('BuildFullTasksList exists' + count);
        //              //console.log(this.TaskSectionsList[key].TS_ID);
        //              //console.log(this.TaskSectionsList[key].TS_Section);

        //              let myNodeList =
        //              {
        //                 id        : 'SECTION-'+this.TaskSectionsList[key].TS_ID,
        //                 type      : 'section',
        //                 title     : this.TaskSectionsList[key].TS_Section,
        //                 notes     : 'test abc.',
        //                 completed : true,
        //                 dueDate   : '2023-09-15T15:12:36.910Z',
        //                 priority  : 0,
        //                 tags      : [
        //                     '2b884143-419a-45ca-a7f6-48f99f4e7798'
        //                 ],
        //                 assignedTo: '3a23baf7-2db8-4ef5-8d49-86d3e708dff5',
        //                 subTasks  : [
        //                     {
        //                         id       : 'f1890ef6-89ed-47ca-a124-8305d7fe71fd',
        //                         title    : 'Sit eu aliqua et et',
        //                         completed: true
        //                     },
        //                 ],
        //                 order     : count
        //             }

        //             this.FullTasksList.push(myNodeList);
        //             count++;
        //             var iTS_ID = this.TaskSectionsList[key].TS_ID
        //             //loop through taskslist
        //             for (var key in this.TasksList) {
                  
        //                 if (this.TasksList.hasOwnProperty(key)) {
        //                     if (this.TasksList[key].Tsk_Section == iTS_ID) {


        //                         let myNodeList2 =
        //                         {
        //                            id        : 'TASK-'+this.TasksList[key].Tsk_ID,
        //                            type      : 'task',
        //                            title     : this.TasksList[key].Tsk_Title,
        //                            notes     : 'test abc.',
        //                            completed : true,
        //                            dueDate   : '2023-09-15T15:12:36.910Z',
        //                            priority  : 0,
        //                            tags      : [
        //                                '2b884143-419a-45ca-a7f6-48f99f4e7798'
        //                            ],
        //                            assignedTo: '3a23baf7-2db8-4ef5-8d49-86d3e708dff5',
        //                            subTasks  : [
        //                                {
        //                                    id       : 'f1890ef6-89ed-47ca-a124-8305d7fe71fd',
        //                                    title    : 'Sit eu aliqua et et',
        //                                    completed: true
        //                                },
        //                            ],
        //                            order     : count
        //                        }
        //                        this.FullTasksList.push(myNodeList2) ;
        //                        count++;
        //                         //console.log('looping taskslist Tsk_Section ' + this.TasksList[key].Tsk_Section + ' '+ count);
        //                     }
        //                 }
        //             }



        //              //{
        //              //   TS_ID: this.TaskSectionsList[key].TS_ID,
        //              //   TS_Section: this.TaskSectionsList[key].TS_Section,

        //              //}



        //           }
        //        }
        //        //console.log(this.FullTasksList);

        //     }
        //   }
        //   )
        // console.log(this.FullTasksList);
        //   return this.FullTasksList ;



    }

    /**
     * Update tasks orders
     *
     * @param tasks
     */
    updateTasksOrders(tasks: Task[]): Observable<Task[]>
    {
        return this._httpClient.patch<Task[]>('api/apps/tasks/order', {tasks});
    }

    /**
     * Search tasks with given query
     *
     * @param query
     */
    searchTasks(query: string): Observable<Task[] | null>
    {
        return this._httpClient.get<Task[] | null>('api/apps/tasks/search', {params: {query}});
    }


    getSectionsList(id):Observable<any[]>{
        return this._httpClient.get<any>(this.API_URL+'/getallsections/'+id);
    }

    addSection(val:any){
        return this._httpClient.post(this.API_URL+'/tasksections/addsection',val);
    }

    getUsersToAssign():Observable<any[]>{
        return this._httpClient.get<any>(this.API_URL+'/tasks/getuserstoassign');
    }
    
    

    /**
     * Get task by id
     */
    getTaskById(id: string): Observable<Task>
    {
        console.log('checking.');
        return this._tasks.pipe(
            take(1),
            map((tasks) => {

                // Find the task
                const task = tasks.find(item => Number(item.id) === Number(id));

                // Update the task
                this._task.next(task);

                // Return the task
                return task;
            }),
            switchMap((task) => {

                if ( !task )
                {
                    return throwError('Could not found task with id of ' + id + '!');
                }

                return of(task);
            })
        );
    }

    /**
     * Create task
     *
     * @param type
     */
    createTask(type: string): Observable<Task>
    {
        return this.tasks$.pipe(
            take(1),
            switchMap((tasks) => this._httpClient.post<Task>('api/apps/tasks/task', {type}).pipe(
                map((newTask) => {

                    // Update the tasks with the new task
                    this._tasks.next([newTask, ...tasks]);

                    // Return the new task
                    return newTask;
                })
            ))
        );
    }

    /**
     * Update task
     *
     * @param id
     * @param task
     * updateTasksOrders(tasks: Task[]): Observable<Task[]>
     */
     updateTask(id: string, task: Task): Observable<Task>
     {
         return this.tasks$
                    .pipe(
                        take(1),
                        switchMap(tasks => this._httpClient.patch<Task>(this.API_URL+'/tasksections1/addupdatetask', {
                             id,
                             task
                        }).pipe(
                            map((updatedTask : Task) => {
 
                                // Find the index of the updated task
                                const index = tasks.findIndex(item => item.id === id);
                                // Update the task
                                tasks[index] = updatedTask;
 
                                // Update the tasks
                                this._tasks.next(tasks);
 
                                // Return the updated task
                                return updatedTask;
                            }),
                            switchMap(updatedTask => this.task$.pipe(
                                take(1),
                                filter(item => item && item.id === id),
                                tap(() => {
 
                                    // Update the task if it's selected
                                    this._task.next(updatedTask);
                                    // Return the updated task
                                    return updatedTask;
                                })
                            ))
                        ))
                    );
     }
 

    /**
     * Save task
     *
     * @param val
     */
          saveTask(val:any){
            return this._httpClient.post(this.API_URL+'/tasksections2/addupdatetask',val);
          }

    /**
     * Delete the task
     *
     * @param id
     */
    deleteTask(id: string): Observable<boolean>
    {
        return this.tasks$.pipe(
            take(1),
            switchMap(tasks => this._httpClient.delete('api/apps/tasks/task', {params: {id}}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted task
                    const index = tasks.findIndex(item => item.id === id);

                    // Delete the task
                    tasks.splice(index, 1);

                    // Update the tasks
                    this._tasks.next(tasks);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
