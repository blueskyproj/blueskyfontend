import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TasksService } from 'app/modules/admin/tasks/tasks.service';
import $ from "jquery";

@Component({
  selector: 'app-add-section-modal',
  templateUrl: './add-section-modal.component.html',
  styleUrls: ['./add-section-modal.component.scss']
})
export class AddSectionModalComponent implements OnInit {

  constructor(public modal:NgbActiveModal, private _tasksService: TasksService,) { }

  ngOnInit(): void {
  }

  addSection(){
    var val = 
        {
          TS_Section:$('#txt_section').val(),
        };  
        this._tasksService.addSection(val).subscribe(res=>{
          Swal.fire({
            title: 'Success',
            text:  "Section added successfully!!",
            icon: 'success'
          }); 
          this.modal.dismiss();
        });  
  }
}