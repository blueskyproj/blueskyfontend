import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatDrawer } from '@angular/material/sidenav';
import { fromEvent, Subject } from 'rxjs';
import { filter, takeUntil, toArray } from 'rxjs/operators';
import { TreoMediaWatcherService } from '@treo/services/media-watcher';
import { TreoNavigationService } from '@treo/components/navigation';
import { Tag, Task } from 'app/modules/admin/tasks/tasks.types';
import { TasksService } from 'app/modules/admin/tasks/tasks.service';
import { SharedService } from 'app/shared.service';
import $ from "jquery";
import { CommonComponent } from '../../../common/common.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddSectionModalComponent } from './add-section-modal/add-section-modal.component';


@Component({
    selector: 'tasks-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksListComponent implements OnInit, OnDestroy {
    @ViewChild('matDrawer', { static: true })
    matDrawer: MatDrawer;

    drawerMode: 'side' | 'over';
    selectedTask: Task;
    tags: Tag[];
    tasks: Task[];
    tasksCount: any;
    newRecordId: number = 0;



    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {DOCUMENT} _document
     * @param {Router} _router
     * @param {TasksService} _tasksService
     * @param {TreoMediaWatcherService} _treoMediaWatcherService
     * @param {TreoNavigationService} _treoNavigationService
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _changeDetectorRef: ChangeDetectorRef,
        @Inject(DOCUMENT) private _document: any,
        private _router: Router,
        private _tasksService: TasksService,
        private _treoMediaWatcherService: TreoMediaWatcherService,
        private _treoNavigationService: TreoNavigationService,
        private service: SharedService,
        private modalService: NgbModal,
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        // Set the defaults
        this.tasksCount = {
            completed: 0,
            incomplete: 0,
            total: 0
        };

        //console.log('constructor');
        this.sFMID = sessionStorage.getItem('FM_ID');
        this.iFMID = Number(sessionStorage.getItem('FM_ID'));
        //this.iFMID = Number(this._activatedRoute.snapshot.paramMap.get("id"));

        this._changeDetectorRef.markForCheck();

    }
    common: any = [];
    TasksList: any = [];
    TaskSectionsList: any = [];
    FullTasksList: any = [];
    iFMID: number;
    sFMID: string;
    FundSummaryList: any = [];
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.common = new CommonComponent();
        this.Start();
    }

    public open(item) {
        const redirectURL = '/fundmaster/' + item
        this._router.navigateByUrl(redirectURL);
    }

    public openinvestors() {
        const redirectURL = '/fundinvestorinfo/' + this.iFMID
        this._router.navigateByUrl(redirectURL);
    }

    public opennotices() {
        const redirectURL = '/fundnotice/' + this.iFMID
        this._router.navigateByUrl(redirectURL);
    }

    Start() {
        //this.GetTasksList(sessionStorage.getItem('U_ID'));
        this.Main();
    }

    Main() {


        // Get the tags
        this._tasksService.tags$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tags: Tag[]) => {
                this.tags = tags;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tasks

        this._tasksService.tasks$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tasks: Task[]) => {
                this.tasks = tasks;
                // Update the counts
                // this.SortedCustomersList.filter(function (item) { return item['id'] == cust})
                this.tasksCount.total = this.tasks.filter(task => task.type === 'task').length - 1;
                this.tasksCount.completed = this.tasks.filter(task => task.type === 'task' && task.completed).length;
                this.tasksCount.incomplete = this.tasksCount.total - this.tasksCount.completed;
                let newtaskid = Number(this.tasks[this.tasksCount.total - 1].id) + 1;
                this.newRecordId = newtaskid;
                // Mark for check
                this._changeDetectorRef.markForCheck();

                // Update the count on the navigation
                /* setTimeout(() => {
 
                     // Get the component -> navigation data -> item
                     const mainNavigationComponent = this._treoNavigationService.getComponent('mainNavigation');
 
                     // If the main navigation component exists...
                     if (mainNavigationComponent) {
                         const mainNavigation = mainNavigationComponent.navigation;
                         const menuItem = this._treoNavigationService.getItem('applications.tasks', mainNavigation);
 
                         // Update the subtitle of the item
                         menuItem.subtitle = this.tasksCount.incomplete + ' remaining tasks';
 
                         // Refresh the navigation
                         mainNavigationComponent.refresh();
                     }
                 });*/
            });

        // Get the task
        this._tasksService.task$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((task: Task) => {
                this.selectedTask = task;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to media query change
        this._treoMediaWatcherService.onMediaQueryChange$('(min-width: 1440px)')
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((state) => {

                // Calculate the drawer mode
                this.drawerMode = state.matches ? 'side' : 'over';

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Listen for shortcuts
        fromEvent(this._document, 'keydown')
            .pipe(
                takeUntil(this._unsubscribeAll),
                filter<KeyboardEvent>((event) => {
                    return (event.ctrlKey === true || event.metaKey) // Ctrl or Cmd
                        && (event.key === '/' || event.key === '.'); // '/' or '.' key
                })
            )
            .subscribe((event: KeyboardEvent) => {

                // If the '/' pressed
                if (event.key === '/') {
                    this.createTask('task');
                }

                // If the '.' pressed
                if (event.key === '.') {
                    this.createTask('section');
                }
            });

    }
    GetTasksList(U_ID) {
        //this.common.showLoader()
        //console.log(U_ID);
        this.service.getTasksList(U_ID).subscribe((data: any[]) => {
            this.TasksList = data;
            //this.common.hideLoader()
            //console.log('Tasklist ' + this.TasksList)
            //this.refreshFundNotice();
            this.BuildFullTasksList(U_ID)
            //this.GetTaskSectionsList(U_ID) ;
        }
        )
    }

    GetTaskSectionsList(U_ID) {
        //this.common.showLoader()
        //console.log(U_ID);
        this.service.getTaskSectionsList(U_ID).subscribe((data: any[]) => {
            this.TaskSectionsList = data['Data'];
            //this.common.hideLoader()
            console.log('TaskSectionlist ' + this.TaskSectionsList)
            //this.refreshFundNotice();
            this.BuildFullTasksList(U_ID)
        }
        )
    }

    BuildFullTasksList(U_ID) {

        this.service.getTaskSectionsList(U_ID).subscribe(data => {
            if (Object.keys(data).length === 0) {
                console.log("no data")
            } else {
                this.TaskSectionsList = data['Data'];
                let count = 0

                for (var key in this.TaskSectionsList) {

                    if (this.TaskSectionsList.hasOwnProperty(key)) {

                        //console.log('BuildFullTasksList exists' + count);
                        //console.log(this.TaskSectionsList[key].TS_ID);
                        //console.log(this.TaskSectionsList[key].TS_Section);

                        let myNodeList =
                        {
                            id: 'SECTION-' + this.TaskSectionsList[key].TS_ID,
                            type: 'section',
                            title: this.TaskSectionsList[key].TS_Section,
                            notes: 'test abc.',
                            completed: true,
                            dueDate: '2023-09-15T15:12:36.910Z',
                            priority: 0,
                            tags: [
                                '2b884143-419a-45ca-a7f6-48f99f4e7798'
                            ],
                            assignedTo: '3a23baf7-2db8-4ef5-8d49-86d3e708dff5',
                            subTasks: [
                                {
                                    id: 'f1890ef6-89ed-47ca-a124-8305d7fe71fd',
                                    title: 'Sit eu aliqua et et',
                                    completed: true
                                },
                            ],
                            order: count
                        }

                        this.FullTasksList.push(myNodeList);
                        count++;
                        var iTS_ID = this.TaskSectionsList[key].TS_ID
                        //loop through taskslist
                        for (var key in this.TasksList) {

                            if (this.TasksList.hasOwnProperty(key)) {
                                if (this.TasksList[key].Tsk_Section == iTS_ID) {


                                    let myNodeList2 =
                                    {
                                        id: 'TASK-' + this.TasksList[key].Tsk_ID,
                                        type: 'task',
                                        title: this.TasksList[key].Tsk_Title,
                                        notes: 'test abc.',
                                        completed: true,
                                        dueDate: '2023-09-15T15:12:36.910Z',
                                        priority: 0,
                                        tags: [
                                            '2b884143-419a-45ca-a7f6-48f99f4e7798'
                                        ],
                                        assignedTo: '3a23baf7-2db8-4ef5-8d49-86d3e708dff5',
                                        subTasks: [
                                            {
                                                id: 'f1890ef6-89ed-47ca-a124-8305d7fe71fd',
                                                title: 'Sit eu aliqua et et',
                                                completed: true
                                            },
                                        ],
                                        order: count
                                    }
                                    this.FullTasksList.push(myNodeList2);
                                    count++;
                                    //console.log('looping taskslist Tsk_Section ' + this.TasksList[key].Tsk_Section + ' '+ count);
                                }
                            }
                        }



                        //{
                        //   TS_ID: this.TaskSectionsList[key].TS_ID,
                        //   TS_Section: this.TaskSectionsList[key].TS_Section,

                        //}



                    }
                }
                //console.log(this.FullTasksList);

            }
        }
        )

        this.Main();


    }




    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to task
     *
     * @param id
     */
    goToTask(id: string): void {
        // Get the current activated route
        let route = this._activatedRoute;
        while (route.firstChild) {
            route = route.firstChild;
        }

        // Go to task
        this._router.navigate(['../', id], { relativeTo: route });

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * On backdrop clicked
     */
    onBackdropClicked(): void {
        // Get the current activated route
        let route = this._activatedRoute;
        while (route.firstChild) {
            route = route.firstChild;
        }

        // Go to the parent route
        this._router.navigate(['../'], { relativeTo: route });

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Create task
     *
     * @param type
     */
    createTask(type: 'task' | 'section'): void {
        // Create the task
        this._tasksService.createTask(type).subscribe((newTask) => {

            // Go to new task
            this.goToTask(newTask.id);
        });
    }

    addSection() {
        const modalRef = this.modalService.open(AddSectionModalComponent, { size: 'md' });
        modalRef.componentInstance.id = 1;
    }

    /**
     * Toggle the completed status
     * of the given task
     *
     * @param task
     */
    toggleCompleted(task: Task): void {
        // Toggle the completed status
        task.completed = !task.completed;

        // Update the task on the server
        this._tasksService.updateTask(task.id, task).subscribe();

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Task dropped
     *
     * @param event
     */
    dropped(event: CdkDragDrop<Task[]>): void {
        // Move the item in the array
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

        // Save the new order
        this._tasksService.updateTasksOrders(event.container.data).subscribe();

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }


}
