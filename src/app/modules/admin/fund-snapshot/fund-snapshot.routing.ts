import { Route } from '@angular/router';
import { FundsnapshotComponent } from 'app/modules/admin/fund-snapshot/fund-snapshot.component';

export const fundRoutes: Route[] = [
    {
        path     : '',
        component: FundsnapshotComponent
    }
];
