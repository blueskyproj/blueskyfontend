import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundsnapshotComponent } from './fund-snapshot.component';

describe('FundsnapshotComponent', () => {
  let component: FundsnapshotComponent;
  let fixture: ComponentFixture<FundsnapshotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundsnapshotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundsnapshotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
