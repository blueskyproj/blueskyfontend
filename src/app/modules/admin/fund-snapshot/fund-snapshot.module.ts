import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
// import { SharedModule } from 'app/shared/shared.module';
import { FundsnapshotComponent } from 'app/modules/admin/fund-snapshot/fund-snapshot.component';
import { fundRoutes } from 'app/modules/admin/fund-master/fund-master.routing';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CommonComponent } from '../../common/common.component';



@NgModule({
    declarations: [
        FundsnapshotComponent , CommonComponent
    ],
    imports     : [
        RouterModule.forChild(fundRoutes),
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatIconModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatStepperModule,
        MatTooltipModule
        // ,
        // SharedModule
    ],
    exports: [
        FundsnapshotModule
    ]
})
export class FundsnapshotModule
{
}

