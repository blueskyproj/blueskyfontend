import { ComponentFixture, TestBed } from '@angular/core/testing';

import {FundmasterComponent } from './fund-master.component';

describe('FundmasterComponent', () => {
  let component: FundmasterComponent;
  let fixture: ComponentFixture<FundmasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FundmasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
