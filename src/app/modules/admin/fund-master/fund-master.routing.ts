import { Route } from '@angular/router';
import { FundmasterComponent } from 'app/modules/admin/fund-master/fund-master.component';

export const fundRoutes: Route[] = [
    {
        path     : '',
        component: FundmasterComponent
    }
];
