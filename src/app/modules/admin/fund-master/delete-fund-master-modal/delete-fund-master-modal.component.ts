import { Component, Input, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { Router} from '@angular/router';
import { SharedService } from 'app/shared.service';

@Component({
  selector: 'app-delete-fund-master-modal',
  templateUrl: './delete-fund-master-modal.component.html',
  styleUrls: ['./delete-fund-master-modal.component.scss']
})

export class DeleteFundMasterModalComponent implements OnInit {
  @Input() id: string;
  sMessage: string;
  messageList: any = [];
  sIcon: string;

  myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });

  constructor(private service: SharedService, public modal: NgbActiveModal, private http: HttpClient , private router: Router) { }

  ngOnInit(): void {
  }


  

  submit() {


    this.service.deleteFundBrokerID(this.id).subscribe(res => {
      // count++;

     });
    
     this.modal.dismiss();
     let currentUrl = this.router.url;
    console.log(currentUrl);
    //console.log("messageList" + this.messageList);

     this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
         this.router.navigate([currentUrl]);
     });
     
  }
}
