import { Component, OnInit, Directive, Input, NgModule, ViewEncapsulation } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import $ from "jquery";
import { CommonComponent } from '../../common/common.component';
import { formatNumber } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeleteFundMasterModalComponent } from './delete-fund-master-modal/delete-fund-master-modal.component';
import { fundMaster } from 'app/models/fundMaster.types';


@Component({
  selector: 'app-fund-master',
  templateUrl: './fund-master.component.html',
  styleUrls: ['./fund-master.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FundmasterComponent implements OnInit {



  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(private service: SharedService,
    private _formBuilder: FormBuilder,
    private _Activatedroute: ActivatedRoute,
    private modalService: NgbModal,
    private _router: Router
  ) {
    this.ISError = false;
  }

  common: any = [];

  verticalStepperForm: FormGroup;

  YesNoList: any = [{ "ID": 0, "Value": "No" }, { "ID": 1, "Value": "Yes" }];

  iFMID: number;

  isLinear = false;

  sFM_ID: string;
  sFM_Name: string;
  sFM_PreviousName: string;
  sFM_Industry_Group: string;
  sFM_Federal_Exemption: string;
  sAdr_ID: string;
  sAdr_Line1: string;
  sAdr_Line2: string;
  sAdr_City: string;
  sAdr_State: string;
  sAdr_Country: string;
  sAdr_Zip: string;
  sAdr_Phone: string;

  sFM_JurisOrg: string;
  sFM_DateOrg: Date;
  sFM_EntityType: string;
  sFM_DateFirstSale: Date;
  sFM_Currency: string;
  sFM_1940ActExclusion: string;
  sFM_MinInvest: string;

  sFM_MatterCode: string;
  sFM_Client: string;
  sFM_BillingPartner: string;
  sFM_PartnerResp: string;
  sFM_Team_Members: string;
  sFM_CIK: string;
  sFM_CCC: string;
  sFM_Passphrase: string;
  sFM_FormDFiling: string;

  sFB_ID: string;
  sFB_FM_ID: string;
  sFB_B_ID: string;
  sB_BrokerName: string;

  sMessage: string;
  ISError: boolean;

  IndustryGroupList: any = [];
  FederalExclusionList: any = [];
  CountryList: any = [];
  CurrencyList: any = [];
  EntityTypeList: any = [];
  BrokersList: any = [];
  FundSummaryList: any = [];
  FundMasterList: fundMaster[];
  AddressList: any = [];
  FundBrokerList: any = [];
  FundBrokerListData: any = [];
  NewFundBrokerList: any = [];
  FundBrokerListNew: any = [];
  FundBrokerInsert: any = [];
  AdditionalData:any=[];


  ItemsIDsList: any = [];


  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.common = new CommonComponent();
    this.iFMID = Number(this._Activatedroute.snapshot.paramMap.get("id"));

    this.loadershow(); 
    
    this.GetIndustryGroupList();

    // Vertical stepper form
    this.verticalStepperForm = this._formBuilder.group({
      step1: this._formBuilder.group({
        sFM_ID: ['0'],
        sFM_Name: ['', Validators.required],
        sFM_PreviousName: [''],
        sFM_Industry_Group: ['', Validators.required],
        sFM_Federal_Exemption: ['', Validators.required],
      }),
      step2: this._formBuilder.group({
        sAdr_ID: [''],
        sAdr_Line1: [''],
        sAdr_Line2: [''],
        sAdr_City: [''],
        sAdr_State: [''],
        sAdr_Country: ['', Validators.required],
        sAdr_Zip: [''],
        sAdr_Phone: ['']
      }),
      step3: this._formBuilder.group({
        sFM_JurisOrg: [''],
        sFM_DateOrg: [''],
        sFM_EntityType: [''],
        sFM_DateFirstSale: [''],
        sFM_Currency: [''],
        sFM_1940ActExclusion: [''],
        sFM_MinInvest: ['0'],
      }),
      step4: this._formBuilder.group({
        sFM_MatterCode: [''],
        sFM_Client: [''],
        sFM_BillingPartner: [''],
        sFM_PartnerResp: [''],
        sFM_Team_Members: [''],
        sFM_CIK: [''],
        sFM_CCC: [''],
        sFM_Passphrase: [''],
        sFM_FormDFiling: ['']
      }),
      step5: this._formBuilder.group({

      }),

    }
    );

  }

  public breadcrumb1() {
    const redirectURL = '/fundsummary'
    this._router.navigateByUrl(redirectURL);
  }


  public openinvestors() {
    const redirectURL = '/fundinvestorinfo/' + this.iFMID
    this._router.navigateByUrl(redirectURL);
  }

  public opennotices() {
    const redirectURL = '/fundnotice/' + this.iFMID
    this._router.navigateByUrl(redirectURL);
  }

  public opentasks() {    
    sessionStorage.setItem('FM_ID', this.iFMID.toString());
    const redirectURL = '/tasks/';
    this._router.navigateByUrl(redirectURL);
  }

  compareFn(option1, option2) {
    if (option2 !== 'undefined') {
      return option1.value === option2.value;
    }

  }

  GetIndustryGroupList() {
    this.service.getIndustryGroupList().subscribe((data: any[]) => {
      this.IndustryGroupList = data;
      this.GetFederalExclusionList();
    }
    )
  }

  GetFederalExclusionList() {
    this.service.getFederalExclusionList().subscribe((data: any[]) => {
      this.FederalExclusionList = data;
      this.GetCountryList();
    }
    )
  }

  GetCountryList() {
    this.service.getCountryList().subscribe((data: any[]) => {
      this.CountryList = data;
      this.GetCurrencyList();
    }
    )
  }

  GetCurrencyList() {
    this.service.getCurrencyList().subscribe((data: any[]) => {
      this.CurrencyList = data;
      this.GetEntityTypeList();
    }
    )
  }

  GetEntityTypeList() {
    this.service.getEntityTypeList().subscribe((data: any[]) => {
      this.EntityTypeList = data;      
      this.GetFundMasterID(this.iFMID);
    }
    )
  }
ng

  GetFundMasterID(FM_ID: number) {
    this.service.getFundMasterID(FM_ID).subscribe((data: any[]) => {
      this.FundMasterList = data['Data'];
      this.AdditionalData=data['AdditionalParameters'];
      if(this.AdditionalData.Message){
        this.ISError = true;
      } else {
        this.ISError = false;
      }      
      if (!FM_ID) {
        this.isLinear = true;
      }
      this.GetAddressID(this.iFMID);
    }
    )
  }

  GetAddressID(FM_ID: any) {
    this.service.getAddressID(FM_ID).subscribe((data: any[]) => {
      this.AddressList = data;
      this.loaderhide(); 
      this.GetBrokersList();
      this.onLoadData();
    }
    )
  }

  GetBrokersList() {
    this.loadershow(); 
    this.FundBrokerListNew = [];
    this.service.getBrokersReverseList(this.iFMID).subscribe((data: any[]) => {
      this.BrokersList = data;

      let SortedListBrokers = [];
      this.BrokersList.forEach(val => {

        let myNodeBroker =
        {
          OvalueBroker: val.B_ID,
          OdisplayBroker: val.B_BrokerName,
          OselectedBroker: false
        }
        SortedListBrokers.push(myNodeBroker);
      });


      if (SortedListBrokers.length === 0) {
        $(".addbrokerdiv").hide();
      } else {
        $(".addbrokerdiv").show();
      }

      this.NewFundBrokerList = {

        sFB_FM_ID: this.iFMID,
        ListBrokers: SortedListBrokers,

      };
      this.FundBrokerListNew.push(this.NewFundBrokerList);



      this.GetFundBrokerID();

    }
    )
  }

  GetFundBrokerID() {
    this.FundBrokerList = [];
    this.FundBrokerListData = [];
    this.service.getFundBrokerID(this.iFMID).subscribe((data: any[]) => {
      if (Object.keys(data['Data']).length === 0 || data['Data'].length === 0 ) {
        this.loaderhide(); 
      } 
      else {
        this.FundBrokerListData = data['Data'];
        let count = 0
        this.FundBrokerListData.forEach(value => {

          this.ItemsIDsList.push(count);
          count++;

          let myNodeList =
          {

            sFB_ID: value.FB_ID,
            sFB_FM_ID: value.FB_FM_ID,
            sFB_B_ID: value.FB_B_ID,
            sB_BrokerName: value.B_BrokerName,
            sB_BrokerState: value.B_BrokerState,
            sB_BrokerCountry: value.B_BrokerCountry,
            sB_BrokerPhone: value.B_BrokerPhone

          }
          this.FundBrokerList.push(myNodeList);
          this.loaderhide(); 
        });
      }
    }
    )
  }

  loadershow()  {
    this.common.showLoader(); 
    $(".main").hide();
  }


  loaderhide()  {
    this.common.hideLoader(); 
    $(".main").show();
  }

  InsertStep1() {

    var val = {
      UserID: sessionStorage.getItem('U_ID'),
      FM_ID: this.sFM_ID,
      FM_Name: this.sFM_Name,
      FM_PreviousName: this.sFM_PreviousName,
      FM_Industry_Group: this.sFM_Industry_Group,
      FM_Federal_Exemption: this.sFM_Federal_Exemption,
      Step: "1"
    };

    this.service.insertUpdateStep1FundMaster(val).subscribe(res => {
      this.sFM_ID = res.toString();
    });
    if (this.sFM_ID === "0") {
      alert("Error");
    }
    else{
      this.isLinear = false;
    }
  }

  InsertStep2() {
    var val = {
      UserID: sessionStorage.getItem('U_ID'),
      Adr_ID: this.sAdr_ID,
      Adr_Type: 1,
      Adr_Default: 1,
      Adr_Link_ID: this.sFM_ID,
      Adr_Line1: this.sAdr_Line1,
      Adr_Line2: this.sAdr_Line2,
      Adr_City: this.sAdr_City,
      Adr_State: this.sAdr_State,
      Adr_Country: this.sAdr_Country,
      Adr_Phone: this.sAdr_Phone,
      Adr_Zip: this.sAdr_Zip,
      Step: "2"

    };

    this.service.insertUpdateStep2FundMaster(val).subscribe(res => {
      this.sMessage = res.toString();
    });
  }

  InsertStep3() {

    var val = {
      UserID: sessionStorage.getItem('U_ID'),
      FM_ID: this.sFM_ID,
      FM_JurisOrg: this.sFM_JurisOrg,
      FM_DateOrg: this.sFM_DateOrg,
      FM_EntityType: this.sFM_EntityType,
      FM_DateFirstSale: this.sFM_DateFirstSale,
      FM_Currency: this.sFM_Currency,
      FM_1940ActExclusion: this.sFM_1940ActExclusion,
      FM_MinInvest: this.sFM_MinInvest.replace(',', ''),
      Step: "3"

    };

    this.service.insertUpdateStep3FundMaster(val).subscribe(res => {
      this.sMessage = res.toString();
    });
  }


  InsertStep4() {
    var val = {
      UserID: sessionStorage.getItem('U_ID'),
      FM_ID: this.sFM_ID,
      FM_MatterCode: this.sFM_MatterCode,
      FM_Client: this.sFM_Client,
      FM_BillingPartner: this.sFM_BillingPartner,
      FM_PartnerResp: this.sFM_PartnerResp,
      FM_Team_Members: this.sFM_Team_Members,
      FM_CIK: this.sFM_CIK,
      FM_CCC: this.sFM_CCC,
      FM_Passphrase: this.sFM_Passphrase,
      FM_FormDFiling: this.sFM_FormDFiling,
      Step: "4"

    };

    this.service.insertUpdateStep4FundMaster(val).subscribe(res => {
      this.sMessage = res.toString();
    });
  }


  InsertStep5() {

    let sFB_B_ID = $("#FundBrokerInsert").val();
    var val = {

      FB_FM_ID: this.iFMID,
      FB_B_ID: sFB_B_ID,

    };

    this.service.InsertFundBroker(val).subscribe(res => {
      this.GetBrokersList();
    });
  }

  addRowBroker() {


    let SortedListBrokers = [];
    this.BrokersList.forEach(val => {

      let myNodeBroker =
      {
        OvalueBroker: val.B_ID,
        OdisplayBroker: val.B_BrokerName,
        OselectedBroker: false
      }
      SortedListBrokers.push(myNodeBroker);
    });

    this.NewFundBrokerList = {

      sFB_FM_ID: this.iFMID,
      ListBrokers: SortedListBrokers,

    };
    this.FundBrokerListNew.push(this.NewFundBrokerList);
    return true;
  }

  onLoadData() {
    console.log("onLoadData");
    var data1 = {
      step1:
      {
        sFM_ID: this.FundMasterList[0].FM_ID,
        sFM_Name: this.FundMasterList[0].FM_Name,
        sFM_PreviousName: this.FundMasterList[0].FM_PreviousName,
        sFM_Industry_Group: Number(this.FundMasterList[0].FM_Industry_Group),
        sFM_Federal_Exemption: Number(this.FundMasterList[0].FM_Federal_Exemption),
      }
    }

    this.verticalStepperForm.patchValue(
      data1
    );
    this.onLoadData2();
  }

  onLoadData2() {
    var data2 = {
      step2:
      {
        sAdr_ID: this.AddressList[0].Adr_ID,
        sAdr_Line1: this.AddressList[0].Adr_Line1,
        sAdr_Line2: this.AddressList[0].Adr_Line2,
        sAdr_City: this.AddressList[0].Adr_City,
        sAdr_State: this.AddressList[0].Adr_State,
        sAdr_Country: Number(this.AddressList[0].Adr_Country),
        sAdr_Zip: this.AddressList[0].Adr_Zip,
        sAdr_Phone: this.AddressList[0].Adr_Phone
      }

    }
    this.verticalStepperForm.patchValue(
      data2
    );

    this.onLoadData3();

    if (typeof data2 !== 'undefined') {
      (<HTMLInputElement>document.getElementById("BtnStep2")).disabled = false;
      (<HTMLInputElement>document.getElementById("BtnStep2")).classList.remove("mat-button-disabled");
    }
  }

  onLoadData3() {
    var str = new String(this.FundMasterList[0].FM_DateFirstSale);
    var newstr = str.split('/').join('-').substring(0, 10)

    var data3 = {
      step3:
      {
        sFM_JurisOrg: this.FundMasterList[0].FM_JurisOrg,
        sFM_DateOrg: this.FundMasterList[0].FM_DateOrg,
        sFM_EntityType: Number(this.FundMasterList[0].FM_EntityType),
        sFM_DateFirstSale: newstr,
        sFM_Currency: this.FundMasterList[0].FM_Currency,
        sFM_1940ActExclusion: this.FundMasterList[0].FM_1940ActExclusion,
        sFM_MinInvest: formatNumber(this.FundMasterList[0].FM_MinInvest, 'en-US', '1.2-2'),
      }
    }
    this.verticalStepperForm.patchValue(
      data3
    );


    this.onLoadData4();

    if (typeof data3 !== 'undefined') {
      (<HTMLInputElement>document.getElementById("BtnStep3")).disabled = false;
      (<HTMLInputElement>document.getElementById("BtnStep3")).classList.remove("mat-button-disabled");
    }
  }

  getstep4() {
    console.log("getstep4");
    this.onLoadData4;
  }

  onLoadData4() {
    var data4 = {
      step4:
      {
        sFM_MatterCode: this.FundMasterList[0].FM_MatterCode,
        sFM_Client: this.FundMasterList[0].FM_Client,
        sFM_BillingPartner: this.FundMasterList[0].FM_BillingPartner,
        sFM_PartnerResp: this.FundMasterList[0].FM_PartnerResp,
        sFM_Team_Members: this.FundMasterList[0].FM_Team_Members,
        sFM_CIK: this.FundMasterList[0].FM_CIK,
        sFM_CCC: this.FundMasterList[0].FM_CCC,
        sFM_Passphrase: this.FundMasterList[0].FM_Passphrase,
        sFM_FormDFiling: this.FundMasterList[0].FM_FormDFiling
      }

    }
    this.verticalStepperForm.patchValue(
      data4
    );
    this.onLoadData5();
    if (typeof data4 !== 'undefined') {
      (<HTMLInputElement>document.getElementById("BtnStep4")).disabled = false;
      (<HTMLInputElement>document.getElementById("BtnStep4")).classList.remove("mat-button-disabled");
    }
  }

  onLoadData5() {
    var data5a = this.FundBrokerList
    var data5b = this.FundBrokerListNew

    this.verticalStepperForm.patchValue(
      data5a
    );

  }

  showDeleteModal(id) {
    const modalRef = this.modalService.open(DeleteFundMasterModalComponent, { size: 'md' });
    modalRef.componentInstance.id = id;
  }

}
