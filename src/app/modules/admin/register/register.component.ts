import { Component, OnInit } from '@angular/core';
import { SharedService } from 'app/shared.service';
import { ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  constructor(private service:SharedService) { 
    this.ISError = false;
  }
  U_FirstName:string;
  U_LastName:string;
  U_Title:string;
  U_Email:string;
  U_Username:string;
  U_Phone:string;
  U_Password:string;
  ISError:boolean;
  ngOnInit(): void {
    
  }

  registerUser() {
    var val = {U_FirstName:this.U_FirstName,
               U_LastName:this.U_LastName,
               U_Title:this.U_Title,
               U_Email:this.U_Email,
               U_Phone:this.U_Phone,
               U_Password:this.U_Password,
               U_Username:this.U_Username};
    this.service.registerUser(val).subscribe(res=>{
       alert(res.toString());
    });
  }
}
