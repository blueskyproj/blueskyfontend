import { Route } from '@angular/router';
import { RegisterComponent } from 'app/modules/admin/register/register.component';

export const regRoutes: Route[] = [
    {
        path     : '',
        component: RegisterComponent
    }
];
