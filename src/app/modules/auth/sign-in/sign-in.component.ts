import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TreoAnimations } from '@treo/animations';
import { AuthService } from 'app/core/auth/auth.service';
import { SharedService } from 'app/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { user } from 'app/data/mock/common/user/data';

@Component({
    selector     : 'auth-sign-in',
    templateUrl  : './sign-in.component.html',
    styleUrls    : ['./sign-in.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : TreoAnimations
})
export class AuthSignInComponent implements OnInit
{
    signInForm: FormGroup;
    message: any;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     * @param {AuthService} _authService
     * @param {FormBuilder} _formBuilder
     * @param {Router} _router
     */
    constructor(
        private service:SharedService,
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router
    )
    {
        // Set the defaults
        this.message = null;
    }

    UserAuthList:any=[];


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

        sessionStorage.clear();
        console.log('sign-in')

        // Create the form
        this.signInForm = this._formBuilder.group({
            email     : [''],
            password  : [''],
            rememberMe: ['']
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    signIn(): void
    {
        // Disable the form
        this.signInForm.disable();

        // Hide the message
        this.message = null;

        // Get the credentials
        const credentials = this.signInForm.value;
        this.service.validateLogin(credentials).subscribe(res=>{
            this.UserAuthList = res;
            if(this.UserAuthList["Status"]==='Success'){

                var iU_ID = this.UserAuthList["U_ID"];
                var sU_FirstName = this.UserAuthList["U_FirstName"];
                var sU_LastName = this.UserAuthList["U_LastName"];
                var sU_Email = this.UserAuthList["U_Email"];
                var sU_Picture = this.UserAuthList["U_Picture"];
                sessionStorage.setItem('U_ID', iU_ID);
                sessionStorage.setItem('U_FirstName', sU_FirstName);
                sessionStorage.setItem('U_LastName', sU_LastName);
                sessionStorage.setItem('U_Email', sU_Email);
                sessionStorage.setItem('U_Picture', sU_Picture);


                user.id = sessionStorage.getItem('U_ID');
                user.name = sessionStorage.getItem('U_FirstName') + ' ' + sessionStorage.getItem('U_LastName');
                user.email = sessionStorage.getItem('U_Email');
                user.avatar = '../../../avatars/avatar_' + sessionStorage.getItem('U_ID') +'.png?' + new Date().getTime() ;
                user.status = 'online';

                const redirectURL = '/dashboard';
                // Navigate to the redirect url
                this._router.navigateByUrl(redirectURL);

            } else if(this.UserAuthList["Status"]==='Failed'){
                
                alert("Invalid login details");
                this.signInForm.enable();
            }            
            
         });
        // Sign in
        /*this._authService.signIn(credentials)
            .subscribe(() => {

                // Set the redirect url.
                // The '/signed-in-redirect' is a dummy url to catch the request and redirect the user
                // to the correct page after a successful sign in. This way, that url can be set via
                // routing file and we don't have to touch here.
                const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/signed-in-redirect';

                // Navigate to the redirect url
                this._router.navigateByUrl(redirectURL);

            }, (response) => {

                // Re-enable the form
                this.signInForm.enable();

                // Show the error message
                this.message = {
                    appearance: 'outline',
                    content   : response.error,
                    shake     : true,
                    showIcon  : false,
                    type      : 'error'
                };
            });*/
    }
}
