/* tslint:disable:max-line-length */
export const user: any = {
    id    : sessionStorage.getItem('U_ID'),
    name  : sessionStorage.getItem('U_FirstName') + ' ' + sessionStorage.getItem('U_LastName'),
    email :  sessionStorage.getItem('U_Email'),
    avatar: '../../../avatars/avatar_' + sessionStorage.getItem('U_ID') +'.png?' + new Date().getTime() ,
    //avatar: '../../../avatars/' + sessionStorage.getItem('U_Picture') ,
    status: 'online'
};
