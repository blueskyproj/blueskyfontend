export interface fundMaster
{

    FM_ID: number;
    FM_M_ID: number;
    FM_Name: string;
    FM_PreviousName?: string;
    FM_Industry_Group?: number;
    FM_Federal_Exemption?: number;   
    FM_MatterCode?: string;
    FM_Client?: string;
    FM_BillingPartner?: string;
    FM_PartnerResp?: string;
    FM_Team_Members?: string;
    FM_CIK?: string;
    FM_CCC?: string;
    FM_Passphrase?: string;
    FM_FormDFiling?: string;
    FM_JurisOrg?: string;
    FM_EntityType?: number;
	FM_DateFirstSale?: Date;
    FM_Currency?: string;
    FM_1940ActExclusion?: number;
    FM_MinInvest?: number;
    FM_DateOrg?: number;
    FM_Status?: number;

}
